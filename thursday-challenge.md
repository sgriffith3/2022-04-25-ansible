# Thursday Challenge

> Requires playbook from Wednesday Challenge

## Part 1

Convert your playbook into a role called nginx_install.

## Part 2

Make a new playbook that calls on the role nginx_install.

