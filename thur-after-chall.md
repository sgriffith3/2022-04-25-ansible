# Thursday Afternoon Challenge

## Part 1
Copy the inventory.py script to netinventory.py.

## Part 2
Then remove the host information for bender and fry, and add in information for sw-1 and sw-2.

## Part 3
Make sure you can run an ansible ping against sw-1 and sw-2 using the netinventory.py script. 
