# Friday Morning Challenge

## Part 1

Do some research into the 'stat' module. Figure out why you might want to use it.

## Part 2

Use the 'stat' module to retrieve the status of a file on the planetexpress hosts. Register the module output.

> Pick/make a file of your own choice. It is more fun if you know it will be on one host, but not on others!

## Part 3

Determine a reason why you might consider a file "failed" (wrong owner, does not exist, etc).

Use a "fail" module to fail the host out if the file fails.

## Part 4 (Advanced)

Add Ansible error handling to your playbook. Use the 'file' module to 'fix' the failed file within the rescue section.
