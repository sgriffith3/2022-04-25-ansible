# Wednesday Challenge

## Part 1

Design a playbook that will apt or yum install nginx, push a config file, and run the commands `sudo nginx -s stop` & `sudo nginx`.

> Hint: get your nginx config file locally with `sudo cp /etc/nginx/nginx.conf ./nginx.conf`

## Part 2

Improve your playbook, making sure to use the following attributes:

1. tasks
2. handlers (only if nginx config file changed)
3. tags

